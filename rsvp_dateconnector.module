<?php
// $Id: rsvp_dateconnector.module,v 1.2.2.11 2009/11/05 22:36:47 ulf1 Exp $


class RsvpDateConnectorImpl extends RsvpConnectorImpl {

  function get_event_field($field_name, $content_type) {
    //$field_name can potentially be empty if no association is available for $content_type.
    if (!isset($field_name)) {
      return array();
    }
    
    $field = field_info_field($field_name);
    if (empty($field)) {
      return array();
    }

    //$field['database'] = content_database_info($field);
    return $field;
  }

  
  function get_datecount($node, $field) {
    if (isset($field['field_name'])){
      return count($node->{$field['field_name']});
    }
    else{
      return 0;
    }
  }

  
  //returns startdate as is to use it as hash
  function get_startdateAsHash($node, $field, $pos) { 
    $date_value = $node->{$field['field_name']}[$node->language][$pos]['value'];
    return $date_value;
  }

  //returns startdate as is to use it as hash
  function get_startdateAsUTC($node, $field, $pos) {

    $date_value = $this->get_startdateAsHash($node, $field, $pos);
    
    $db_tz = $this->_get_dbtimezone($node, $field, $pos);

    // Create a date object
    $date = new DateObject($date_value, $db_tz);
    // Make sure the date object is going to print UTC values.
    $date->setTimezone(timezone_open('UTC'));

    $date_utc = $date->format('U');

    return $date_utc;

  }
  
  
  //returns startdate as string in local timezone
  function get_startdateAsString($node, $field, $pos) {
    return $this->format_date($node, $field, $pos, FALSE);
  }
  
  //returns enddate in utc form
  function get_enddateAsUTC($node, $field, $pos) {

    if ($this->has_enddate($node, $field, $pos) == true){
      
      $date_value = $node->{$field['field_name']}[$pos]['value2'];

      $date = new DateObject($date_value, $db_tz);

      // Create a date object
      $date = date_make_date($date_value, $db_tz, $field['type']);
      // Make sure the date object is going to print UTC values.
      $date->setTimezone(timezone_open('UTC'));

      $date_utc = $date->format('U');

      return $date_utc;
    }
    else {
      return null;
    }
  }
  
  //returns if event has a valid enddate
  function has_enddate($node, $field, $pos) {
    return isset($node->{$field['field_name']}[$pos]['value2']);
  }
  
  //returns the position for a particular hash, or -1 if invalid hash
  function get_posByHash($node, $field, $hash) {

    $count = $this->get_datecount($node, $field);

    for ($i = 0; $i < $count; $i++) {
      $unixdate = $this->get_startdateAsHash($node, $field, $i);
      if ($unixdate == $hash) {
        return $i;
      }
    }
    return -1;
  }
  
    
  function is_event_enabled($contenttype) {
    return true;
  }
  
  function isTypesAreSelectable() {
    return true;
  }
  
  //returns true if the connector supports multiple dates per field per content-type. (e.g. Repeatable dates in Date API).
  function hasMultipleDatesPerField() {
    return true;
  }
  
  // internal function
  function _get_dbtimezone($node, $field, $pos) {
    // Figure out the timezone handling for this date.
    if ($field['tz_handling'] == 'date') {
      $tz = $node->{$field['field_name']}[$pos]['timezone'];
    }
    else {
      $tz = date_default_timezone();
    }
    $db_tz = date_get_timezone_db($field['tz_handling'], $tz);
    
    return db_tz;
  }
  
  function format_date($node, $field, $pos, $include_to_date = FALSE){
    
    //get default format (set in "Default Display" in the global section of the "Managing Field")
    $format = date_formatter_format('date_default', $field['field_name'], date_granularity($field));
    
    // Figure out the timezone handling for this date.
    if ($field['settings']['tz_handling'] == 'date') {
      $tz = $node->{$field['field_name']}[$node->language][$pos]['timezone'];
    }
    else {
      $tz = date_default_timezone();
    }
  
    $display_tz = date_get_timezone($field['settings']['tz_handling'], $tz);
    $db_tz = date_get_timezone_db($field['settings']['tz_handling'], $tz);
    if (isset($node->{$field['field_name']})) {
      $date_value = $node->{$field['field_name']}[$node->language][$pos]['value'];
    }
    else {
      $date_value = $node->{$field['database']['columns']['value']['column']};
    }
    $date = new DateObject($date_value, $db_tz);
    if ($db_tz != $display_tz) {
      $date->setTimezone(timezone_open($display_tz));
    }
    $date_out = date_format_date($date, 'custom', $format);
    
    if ($include_to_date && $this->has_enddate($node, $field, $pos)) {
      if (isset($node->{$field['field_name']})) {
       $date_value = $node->{$field['field_name']}[$node->language][$pos]['value2'];
      }
      else {
        $date_value = $node->{$field['database']['columns']['value2']['column']};
      }
      $date = new DateObject($date_value, $db_tz);
      if ($db_tz != $display_tz && $date) {
        $date->setTimezone($date, timezone_open($display_tz));
      }

      if ($date_value) {
        $date_out .= t(' to ') . date_format_date($date, 'custom', $format);
      }
    }
    return $date_out;
  }
}


function rsvp_dateconnector_getconnector() {
  return new RsvpDateConnectorImpl();  
}
